module.exports = {
	Plans: require('./PlansStore'),
	Schedules: require('./SchedulesStore'),
	Sensors: require('./SensorsStore'),
	Settings: require('./SettingsStore'),
	Switcher: require('./SwitcherStore'),
	TempChecker: require('./TempCheckerStore'),
	Zones: require('./ZonesStore')
};
